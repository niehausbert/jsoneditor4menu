  //-----CONSTRUCTOR----JS2UML---------------  // adding a constructors and libraries to make 'require' happy ...


	function JSZip () {};

	function LinkParam () {};

	//----------------------------------------------
	// JQUERY is required for JS2UML Constructor
	var jsdom = require("jsdom");
	const { JSDOM } = jsdom;
	const { window } = new JSDOM();
	const { document } = (new JSDOM('')).window;
	global.document = document;

	var $ = jQuery = require('jquery')(window);

/*
JSONEditor.defaults.theme = 'bootstrap3';
JSONEditor.defaults.iconlib = 'fontawesome4';
JSONEditor.plugins.ace.theme = 'xcode';
*/

function JSONEditor4Menu () {
  //---- attributes ----
  this.aLinkParam = new LinkParam();
  this.aDoc = null; // stores the "document" object init class with initDoc()
  // var editor = new JSONEditor4Menu();
  // editor.initDoc(document)
  this.aJSON = {};
  this.aZIP = new JSZip();
  this.aDefaultJSON = {};
  this.aSchema = null;
  this.aOptions = {
    "editor_id": "editor_holder",
    "validator_id":"valid_indicator",
    "load_file_id" : "load_filename",
    "filename_id" : "display_filename", // id to display the filename
    "filename_key" : "filepath", // key to access the output file for Handlebars3Code template generation
    "out_json": "tOutJSON",
    "out_code": "tOutput",
    "out_errors": "tErrors",
    "theme": 'bootstrap3',
    "iconlib": 'fontawesome4',
    "plugins":{
      "ace":{
        "theme": 'xcode'
      }
    }
  };
  this.aSettingsBOOL = false;
  this.aEditor = null;
  //----  methods ----
  this.initDoc = function (pDoc) {
    this.aDoc = pDoc;
    //this.aDoc.JSONEditor = JSONEditor;
  };

  this.initJSON = function () {
    var vName = vFileBase;
    console.log("Init with aDefaultJSON in JSON Editor stored in 'docs/db/menu_default.js'");
    this.aEditor.setValue(this.aDefaultJSON);
  };

  this.initZIP = function (pZIP) {
    // the aZIP will be populated with the generated HTML, CSS and Javascript files
    console.log("Init Output ZIP in JSON Editor");
    this.aZIP = pZIP;
  };


  this.getLS4JSON = function () {
      console.log("CALL: initLS4JSON() init JSON from Local Storage");
      var vName = vFileBase || "JSON4LS";
      var vJSON_LS = null;
      var vLS_ID = this.url2id();
      if (typeof(localStorage.getItem(vLS_ID)) !== undefined) {
          console.log("CALL: initLS4JSON() - try init JSON Editor from Local Storage ");
          var vJSONstring = localStorage.getItem(vLS_ID);
          if (!vJSONstring) {
            console.log("CALL: initLS4JSON() - JSON-DB '"+vName+"' undefined in Local Storage.\nSave default as JSON");
          } else {
            console.log("CALL: initLS4JSON() - parse DB '"+vName+"') from LocalStorage JSONstring='"+vJSONstring.substr(0,120)+"...'");
            try {
                vJSON_LS = JSON.parse(vJSONstring);
                if (vJSON_LS) {
                  console.log("CALL: initLS4JSON() - Successful update of JSON Editor from Localstorage ID='" + vLS_ID + "'");
                } else {
                  console.error("CALL: initLS4JSON() - parsed JSON in initJSON4LS() undefined 'vJSON_JS' from LocalStorage");
                }
            } catch(e) {
                alert("ERROR CALL initLS4JSON():" + e);
            }
          }
        }
        return vJSON_LS;
  };

  this.init = function (pInitJSON,pDefaultJSON,pSchema,pTemplates,pOptions) {
      // LOAD PRIORITY
      // (1) jsondata in Link Parameter
      // (2) vJSON is loaded from LocalStorage
      // (3) pInitJSON if init/demo data provide by constructor
      // (4) pDefaultJSON as initialized with default data on delete
      this.aLinkParam.init(this.aDoc);
      var vJSON = pDefaultJSON;
      var vJSON4LinkParam = this.loadLinkParam("jsondata");
      var vJSON4LS = this.getLS4JSON();
      if (vJSON4LinkParam) {
        // (1) highest priority: jsondata in Link Parameter
        console.log("CALL: JSONEditor4Menu.init() - init with LinkParam JSON data");
        vJSON = vJSON4LinkParam;
      } else if (vJSON4LS) {
        // (2) medium priority: jsondata loaded from LocalStorage if saved
        console.log("CALL: JSONEditor4Menu.init() - init JSON Editor with LocalStorga JSON data");
        vJSON = vJSON4LS;
      } else if (pInitJSON) {
        // (3) pInitJSON if init/demo data provide by constructor init call
        console.log("CALL: JSONEditor4Menu.init() - use init data in pJSON for JSON editor");
        vJSON = pInitJSON;
      } else {
        // (3) pJSON as initialized with default data
        console.log("CALL: JSONEditor4Menu.init() - use default data in pDefaultJSON - also used by init_ask() method.");
        if (pDefaultJSON) {
          vJSON = pDefaultJSON;
          console.log("pDefaultJSON defined in JSONEditor4Menu.init()");
        } else {}
          console.error("WARNING: pDefaultJSON undefined - use an empty JSON");
          vJSON = {
            data: {},
            settings: {}
          };
      }
      this.aJSON = vJSON;
      console.log("HTML-INIT init_definitions(pJSON,pSchema)): "+JSON.stringify(vJSON,null,4));
      this.aDefaultJSON = pDefaultJSON;
      // extend/overwrite options
      this.aOptions = pOptions;
      this.aTemplates = pTemplates;
      this.aSchema = pSchema;

      console.log("HTML-INIT (1) JSONEditor4Menu.init(...)): vJSON.settings="+JSON.stringify(vJSON.settings,null,4));
      //PARMETER SCOPEERROR: do not provide attributes with parameter of methods - use aSchema and aJSON instead
      //DO NOT USE: this.aSchema = this.init_definitions(vJSON,pSchema);
      this.init_definitions();
      // Extend aOptions with settings in pOption
      for (var iKey in pOptions) {
        if (pOptions.hasOwnProperty(iKey)) {
          this.aOptions[iKey] = pOptions[iKey];
        }
      }
      // COMPILE the templates with Handlebars
      //this.aSchema = vSchema;
      this.create_compiler4tpl();
      this.create_editor();
      this.aDoc.JSONEditor = JSONEditor; //assign to document.JSONEditor
      this.update_filename();
    };


  this.loadLinkParam = function (pLSID) {
    var vDataID = pLSID || "jsondata";
    var vJSON = null;
    var vJSONstring = "";
    console.log("loadLinkParam('"+vDataID+"')");
    //console.log("Start JSON:\n"+JSON.stringify(vJSON,null,3));
    //-------------------------------------------------------
    // LOCAL STORAGE: Check JSON Data is available in LocalStorage
    //console.log("loadParamStorage(pInitJSON,'"+vLSID+"' - JSON:\n"+(JSON.stringify(vJSON,null,3)).substr(0,120)+"...");
    //-------------------------------------------------------
    // LINK PARAMETER: Evaluation link parameter in JSON Path
    if (this.aLinkParam.exists(vDataID)) {
       console.log("LinkParameter provides '"+vDataID+"'  with value");
       vJSONstring = this.aLinkParam.getValue(vDataID);
       try {
         vJSON = JSON.parse(vJSONstring);
       } catch (e) {
         console.log("ERROR (JSON in LinkParam['"+vDataID+"']: "+e);
         vJSON = null;
       }
       if (vJSON) {
         console.log("LinkParam['"+vDataID+"']: JSON set to this.aJSON:\n"+JSON.stringify(vJSON.settings,null,4));
       }
     } else {
       console.log("LinkParam['"+vDataID+"'] does not contain data.");
    }
    return vJSON;
  };

  this.submit2callback = function(pLink) {
    var vJSONstring = JSON.stringify(this.getValue());
    var vLink = "receiver.html"; // is a default HTML as callback
    // to check the LinkParam communication between HTML documents
    if (pLink) {
      vLink = pLink;
    } else {
      if (this.aLinkParam.exists("callback")) {
        vLink = this.aLinkParam.getValue("callback");
        console.log("Callback defined in LinkParam:\n  "+vLink);
      }
    }
    this.aLinkParam.setValue("jsondata",vJSONstring);
    this.aLinkParam.deleteValue("callback");
    // send current JSON data back to callback URL
    document.location.href = vLink + this.aLinkParam.getParam4URL();
  };

  /*
  el-method is used to replace calls
  document.getElementById
  */
  this.el = function (pID) {
    return this.aDoc.getElementById(pID);
  };
/*
  defined in /src/libs/handlebars_helpers

  function compileCode(pTplID,pJSON) {
    // pJSON is JSON data of the UML Class
    var vCode = vCodeCompiler[pTplID](pJSON);
    vCode = postProcessHandlebars(vCode,pJSON);
    return vCode;
  };
  */

  this.compileCode = {};

  this.getEditor = function(pEditorID) {
      var vEditor = null;
      if (this.aEditor) {
        e = this.aEditor.getEditor(pEditorID);
        if (e) {
          vEditor = e;
        } else {
          console.log("ERROR: JSONEditor4Menu.getEditor('"+pEditor+"') - Editor not found!");
        }
      }
      // return the editor with the ID pEditorID
      return vEditor;
  };


    this.append_classlist = function (pList,pAppendList) {
      if (isArray(pList)) {
        if (isArray(pAppendList)) {
          if (pAppendList) {
            for (var i = 0; i < pAppendList.length; i++) {
              if (isHash(pAppendList[i])) {
                if (pAppendList[i].hasOwnProperty('name')) {
                  pList.push(pAppendList[i].name);
                } else {
                  console.log("ERROR: pAppendList[i].name has no 'name' property!");
                }
              } else {
                pList.push(pAppendList[i]);
                console.log("WARNING: pAppendList[i] is not a hash with attribute 'name'!");
              }
            }
          } else {
          console.log("ERROR: append_classlist() - pAppendList is not an array!");
          }
        } else {
          console.log("ERROR: append_classlist() - pList is not an array!");
        }
      }
    };

    this.init_definitions = function () {
      var vJSON = this.aJSON;
      if (this.aEditor) {
        vJSON = cloneJSON(this.aEditor.getValue());
      } else {
        console.log("JSONEditor undefined");
      };
      console.log("Call: init_definitions() Update Filename in Schema - update filename");
        this.update_filename(); // update the filename in the DOM node with id "load_filename"
        console.log("HTML-INIT init_definitions(pJSON,pSchema)): vJSON.settings="+JSON.stringify(vJSON.settings,null,4));
        if (!vJSON) {
          console.log("WARNING: src/exportmod.js - init_definitions() - vJSON undefined!");
          alert("WARNING: src/exportmod.js - init_definitions() - vJSON undefined!");
        } else if (!(vJSON.settings)) {
          console.log("WARNING: src/exportmod.js - init_definitions() - vJSON.settings undefined!");
          alert("WARNING: src/exportmod.js - init_definitions() - vJSON.settings undefined!");
        } else {
            // use always one blank for "no class" otherwise value is regarded as undefined.
            var watchclasses = [" "]; //
            var i = 0;
            console.log("Call: init_definitions() ");
            // BASIC CLASSES: push all basic classes
            var cl = vJSON.settings.baseclasslist;
            console.log("Call: init_definitions() - BaseClassList: "+JSON.stringify(cl,null,4));
            this.append_classlist(watchclasses,cl);
            // LOCAL CLASSES: push all local classes
            cl = vJSON.settings.localclasslist;
            console.log("Call: init_definitions() - LocalClassList: "+JSON.stringify(cl,null,4));
            this.append_classlist(watchclasses,cl);
            // REMOTE CLASSES: push all remote classes
            console.log("Call: init_definitions() - RemoteClassList: "+JSON.stringify(cl,null,4));
            cl = vJSON.settings.remoteclasslist;
            this.append_classlist(watchclasses,cl);
            watchclasses.sort();
            console.log("Call: init_definitions() - watchclasses=('"+watchclasses.join("','")+"')");
            this.aSchema.definitions.selectorclass.enum = watchclasses;
        }
        //PARAM SCOPE WARNING: do not return an attribute of "this" instance - operated on this.aSchema instead;
        //DO NOT: return pSchema
  };

  // create the Handlebars compiler function from templates in this.aTemplates
  this.create_compiler4tpl = function () {
    var vTemplate = " ";
    for (var tplID in this.aTemplates) {
      if (this.aTemplates.hasOwnProperty(tplID)) {
        console.log("Compile Template ["+tplID+"]");
        vTemplate = this.aTemplates[tplID];
        //vTemplate = preProcessHandlebars(vTemplate,this.aJSON);
        this.compileCode[tplID] = Handlebars.compile(vTemplate);
      }
    }
  };

  this.create_editor = function (pJSON) {
    if (pJSON) {
      this.aJSON = pJSON;
    }
    // If an old editor exists - destroy the Editor to free resources
    if (this.aEditor) {
        /*
        this.aJSON = this.aEditor.getValue();
        this.saveLS("jsondata");
        */
        // this.update_watchclasslist();
        // free some resources if the editor already exists
        this.aEditor.destroy();
        console.log("Destroy JSONEditor in JSONEditor4Menu");
    } else {
      console.log("REMARK: instance of JSON Editor does not exist!");
    }

    console.log("CALL: create_editor() - create a new JSONEditor() in JSONEditor4Menu");
    // update schema
    console.log("Start Editor with JSON:\n"+JSON.stringify(this.aJSON,null,3));
    var vEditorNode = this.el(this.aOptions.editor_id);
    if (vEditorNode) {
      //vEditorNode.innerHTML = " ";
      console.log("CALL: Editor Node cleaned innerHTML");
    } else {
      console.log("CALL: Editor Node does not exist!");
    }
    this.aEditor = new JSONEditor(vEditorNode,{
            // Enable fetching schemas via ajax
            ajax: true,

            // The schema for the editor
            schema: this.aSchema,

            // Seed the form with a starting value
            startval: this.aJSON,

            // theme of JSON editor
            theme: this.aOptions.theme || "bootstrap3",
            // iconlib
            iconlib: this.aOptions.iconlib || "fontawesome4",
            // theme of ACE code editor
            plugins: this.aOptions.plugins || {
                ace: {
                  theme: 'xcode'
                }
            },
            // Disable additional properties
            no_additional_properties: true,

            // Require all properties by default
            required_by_default: true
          });
    this.parent_editor = this;
    this.init_buttons();
    this.init_watch();
    this.update_filename();
    this.update_modified();
    this.saveLS("jsondata");
    this.aDoc.JSONEditor = JSONEditor; //assign to document.JSONEditor

  };

  this.init_ask = function () {
    var vOK = confirm("DELETE: Do you really want to initialize the JSON data for '" + this.getAppName() + "'?");
    if (vOK == true) {
    		var vSaveOK = confirm("SAVE: Do you want to SAVE the current JSON data of '" + this.getAppName() + "' first?");
    		if (vSaveOK == true) {
    			this.saveJSON();
    			console.log("JSON-DB initalized with data for '"+this.getAppName()+"'!");
    		} else {
    			console.log("JSON-DB for  '"+this.getAppName()+"' not saved - data deleted!");
        }
        var vInitOK = confirm("DEMO-DATA: Do you want to initalize with a DEMO of '"+getAppName(this.aInitJSON)+"' first?");
    		if (vInitOK == true) {
    			this.aEditor.setValue(this.aInitJSON);
    			console.log("JSON-DB initalized with Demo JSON '"+this.getAppName(this.aInitJSON)+"'!");
    		} else {
          this.aEditor.setValue(this.aDefaultJSON); // defined e.g. in /db/uml_default.js
      		console.log("JSON-DB for '"+this.getAppName(this.aDefaultJSON)+"' initialized with default values - data deleted!");
        }
        this.saveLS("jsondata");
        this.update_filename();
        this.update_modified();
    } else {
        console.log("initialize JSON-DB cancelled");
    }
  };

  this.delete_ask = function () {
    var vOK = confirm("DELETE: Do you want to delete all data?");
    if (vOK == true) {
        var vSaveOK = confirm("SAVE: Do you want to save the current data for '"+this.getAppName()+"' first?");
        if (vSaveOK == true) {
          this.saveJSON();
          console.log("JSON-DB initalized with data for '"+this.getAppName()+"'!");
        } else {
          console.log("JSON-DB for app '"+this.getAppName()+"' not saved - data deleted!");
        }
        var vInitOK = confirm("DEMO-DATA: Do you want to initalize with a DEMO of '"+getAppName(this.aInitJSON)+"' first?");
        if (vInitOK == true) {
          this.aEditor.setValue(this.aInitJSON);
          console.log("JSON-DB initalized with Demo JSON '"+this.getAppName(this.aInitJSON)+"'!");
        } else {
          var vEmptyJSON = {
              "data":{
                "appname":"MyWebApp"
              },
              "settings":(this.aJSON.settings || {})
          };
          this.aEditor.setValue(vEmptyJSON);
          console.log("JSON-DB for UML class '"+this.getAppName(vEmptyJSON)+"' not saved - data deleted!");
        }
        localStorage.clear();
        this.saveLS("jsondata");
        this.update_filename();
        this.update_modified();
        console.log("JSON-DB deleted'!");
        //save changes to Local Storage
    } else {
        console.log("initialize JSON-DB cancelled");
    }
  };

  this.showEditor = function (pEditorID,pBoolean) {
    var self = this.getEditor(pEditorID);
    //if (self.collapsed) {
    if (self) {
      if (pBoolean == true) {
        self.editor_holder.style.display = '';
        self.collapsed = false;
        self.setButtonText(self.toggle_button, '', 'collapse', self.translate('button_collapse'));
      } else {
        self.editor_holder.style.display = 'none';
        self.collapsed = true;
        self.setButtonText(self.toggle_button, '', 'expand', self.translate('button_expand'));
      }
    } else {
      console.log("ERROR: showEditor('"+pEditor+"',pBoolean) Editor for ['"+pEditorID+"'] not found");
    }
  };

  this.toggleSettings = function (pSettingsID,pDataID) {
    // if(editor.getEditor('root.location').isEnabled()) alert("It's editable!");
    // Check if Settings are enabled
    //if (this.options.collapsed) {
    //  $trigger(this.toggle_button,'click');
    //}
    this.update_modified();
    // update the class watchlist stored in Schema at "definitions.selectorclass"
    this.init_definitions();
    // restart the editor
    var vJSON = cloneJSON(this.aEditor.getValue());
    this.create_editor(vJSON);
    //this.init(this.aJSON,this.aDefaultJSON,this.aSchema,this.aTemplates,this.aOptions);
    console.log("CALL: this.create_editor() in toggleSettings('" + pSettingsID + "','" + pDataID + "')");
    // set the Editor properly
    if (this.aSettingsBOOL == false) {
      alert("JSON-Editor: Show Settings");
      this.showEditor(pSettingsID,true);
      this.showEditor(pDataID,false);
    } else {
      alert("JSON-Editor: Hide Settings");
      this.showEditor(pSettingsID,false);
      this.showEditor(pDataID,true);
    }
    this.aSettingsBOOL = !this.aSettingsBOOL;
  };

  this.toggleEnable = function () {
    if (this.aEditor.isEnabled()) {
      this.aEditor.disable();
    } else {
      this.aEditor.enable();
    }
  };

  this.enable = function (pID) {
    if (pID) {
      this.aEditor.getEditor(pID).enable();
    } else {
      this.aEditor.enable();
    }
  };

  this.disable = function (pID) {
    if (pID) {
      this.aEditor.getEditor(pID).disable();
    } else {
      this.aEditor.disable();
    }
  };

  this.init_buttons = function () {
    var vThis = this; // "vThis" used because "this" is not available in function
    this.set_button_click("submit",function() {
        // Get the value from the editor
        var vContent = JSON.stringify(vThis.aEditor.getValue(),null,4);
        vThis.el("tJSON");
        console.log("JSON Data:\n"+vContent);
    });
    this.set_button_click("enable_disable",function() {
        if (vThis.aEditor) {
            // Enable form
            if(!vThis.aEditor.isEnabled()) {
                vThis.aEditor.enable();
            }
            // Disable form
            else {
                vThis.aEditor.disable();
            }
        }
    });
  };



  this.init_watch = function () {
    console.log("CALL: init_watch() ");
    // watch event is fired before the value is changed,
    // so getValue() provides the value before alteration of JSON.
    // USE: onchange event with editor.on
    var vThis = this; // "vThis" used because "this" is not available in function
    if (this.aEditor) {
      this.aEditor.watch('root.data.appname',function() {
        console.log("Watch-Event 'root.data.appname'");
        vThis.update_filename();
        //update_editor();
      });
      /*
      var vEditor = this.aEditor;
      if (vEditor) {
        if (typeof vEditor.on === 'function') {
          vEditor.on('change',function() {
            console.log("onchange-Event 'root.data.appname'");
            vThis.update_filename();
          });
        } else {
          console.log("WARNING: In JSON-Editor ['"+vEditID+"'] on-Method not defined!");
        }
      };

      this.aEditor.watch('root.data.appname',function() {
        console.log("Watch-Event 'root.data.appname'");
        vThis.update_filename();
        //update_editor();
      });

      this.aEditor.watch('root.settings.baseclasslist',function() {
        vThis.update_watchclasslist();
        //update_editor();
      });
      this.aEditor.watch('root.settings.localclasslist',function() {
        vThis.update_watchclasslist();
        //update_editor();
      });
      this.aEditor.watch('root.settings.remoteclasslist',function() {
        vThis.update_watchclasslist();
        //update_editor();
      });
      */
      //this.start_watch()
    } else {
      console.log("aEditor not defined in init_watch()-call");
    }
  };

  this.start_watch = function () {
    var vThis = this; // "vThis" used because "this" is not available in function
    if (this.aEditor) {
      console.log("start_watch()-call");
      this.aEditor.on('change',function() {
        vThis.validate_errors();
        //vThis.saveLS("jsondata");
        vThis.update_filename();
        //update_editor();
        vThis.update_modified();
        // update_modified date
      });
    }
  };

  this.stop_watch = function () {
    var vThis = this; // "vThis" used because "this" is not available in function
    console.log("stop_watch()-call");
    if (this.aEditor) {
      this.aEditor.on('change',function() {
        //update_editor();
      });
    }
  };

  // ---- getElementById call ---
  this.el = function (pID) {
    return this.aDoc.getElementById(pID);
  };

  this.set_button_click = function (pID,pFunction) {
    var vNode = this.el(pID);
    if (vNode) {
        vNode.addEventListener('click',pFunction);
    } else {
        console.log("DOM node ["+pID+"] does not exist. Could not assign");
    }
  };

  this.update = function () {
    alert("update Schema changes for the JSONEditor4Menu");
    this.create_editor();
  };

  this.getValue = function () {
    var vJSON = this.aJSON;
    if (this.aEditor) {
      vJSON = cloneJSON(this.aEditor.getValue());
    } else {
      console.log("this.aEditor undefined in JSONEditor4Menu.getValue()");
    }
    return vJSON;
  };

  this.setValue = function (pJSON) {
    this.aJSON = pJSON;
    if (this.aEditor) {
      this.aEditor.setValue(pJSON);
      console.log("setValue() executed!\n"+JSON.stringify(pJSON,null,4));
    } else {
      console.log("this.aEditor undefined in JSONEditor4Menu.setValue(pJSON)");
    }
  };


  this.update_modified = function () {
      this.aJSON = this.getValue();
      if (this.aJSON) {
        if (this.aJSON.settings) {
          this.aJSON.settings.modified = getDateTime();
          this.setValue(this.aJSON);
          console.log("settings.modified updated with: '"+this.aJSON.settings.modified+"'");
        } else {
          console.log("this.aJSON.settings.modified was undefined - src/libs/exportmod.js:518");
        }
        this.update_filename();
      }
  };

  this.update_filename = function () {

    if (this.aOptions.hasOwnProperty("filename_id")) {
      var vNode = this.el(this.aOptions.filename_id); // e.g. filename_id = "load_filename";
      var vJSON = this.aJSON;
      if (vNode) {
        if (this.aEditor) {
          console.log("CALL: update_filename() - use vJSON = this.aEditor.getValue()!");
          vJSON = cloneJSON(this.aEditor.getValue());
        } else {
          console.log("CALL: update_filename() - this.aEditor not defined!");
        }
      }

      var vDOMID = this.aOptions.filename_id;
      if (this.el(vDOMID)) {
        if (vJSON.data) {
          if (vJSON.data.hasOwnProperty("appname")) {
            console.log("CALL: update_filename('"+vJSON.data.appname+"') - Name of class was updated in DOM node ["+vDOMID+"]!");
            vNode.innerHTML = app2filename(vJSON.data.appname)+vJSON.settings.extension4json;
          } else {
            console.log("WARNING: update_filename() - Attribute 'appname' in 'this.aJSON.data.appname' - was not defined!");
          }
        } else {
          console.log("WARNING: update_filename() - this.aJSON does not contain the hash 'this.aJSON.data' for access to classname 'this.aJSON.data.appname' - update of classname in DOM was not performed!");
        }
      } else {
        console.log("WARNING: update_filename() - DOM node ["+vDOMID+"] does not exist!");
      }
    } else {
      console.log("WARNING: update_filename() - DOM-ID is not defined in this.aOptions['filename_id']");
    }
  };


  this.update_subeditor = function (pEditPath,pJSON) {
    var ed_classlist =  this.getEditor(pEditPath);
    // `getEditor` will return null if the path is invalid
    var s = this.aJSON.settings;
    if (ed_classlist) {
      ed_classlist.setValue(pJSON);
      console.log("update_subeditor('"+pEditPath+"',pJSON) "+ed_classlist.getValue());
    } else {
      console.log("update_subeditor('"+pEditPath+"',pJSON) editor undefined - wrong Edit Path");
    }
  };

  this.validate_errors = function () {
    // Get an array of errors from the validator
    var errors = this.aEditor.validate();

    var indicator = this.el(this.aOptions.validator_id);

    // Not valid
    if(errors.length) {
      indicator.style.color = 'red';
      indicator.textContent = "not valid";
    }
    // Valid
    else {
      indicator.style.color = 'green';
      indicator.textContent = "valid";
    }
    var vErrors = "";
    var vCR = "";
    for (var i = 0; i < errors.length; i++) {
      vErrors +=  vCR + errors[i].path + " - " +errors[i].property +" - "+errors[i].message;
      vCR = "\n";
    }
    this.el("tErrors").value = vErrors;
  };

  this.url2id = function (pPrefix) {
      var vPrefix = pPrefix || "";
      var url = document.location.href;
      var id = url.replace(/^https:\/\//g,"");
      id = id.replace(/^http:\/\//g,"");
      id = id.replace(/^file:\/\//g,"");
      id = id.replace(/[^A-Za-z0-9]/g,"x");
      console.log("url2id='" + id + "'");
      return id;
  };

  this.loadLS = function (pLSID) {
    var vLSID = this.url2id() || pLSID || "jsondatra"; //this.aJSON.data.appname;
    var vJSONstring = "";
    if (typeof(Storage) != "undefined") {
        // Store
        if (typeof(localStorage.getItem(vLSID)) !== undefined) {
          console.log("CALL: loadLS() - LocalStorage: '"+vLSID+"' try loading from Local Storage");
          vJSONstring = localStorage.getItem(vLSID);
          if (!vJSONstring) {
            console.log("CALL: loadLS() - LocalStorage: '"+vLSID+"' undefined in Local Storage.\nSave default as JSON");
            //vJSONstring = JSON.stringify(this.getValue());
            //console.log("LocalStorage: loadLS('"+vLSID+"') - init with JSONstring='"+vJSONstring.substr(0,120)+"...'");
            //localStorage.setItem(vLSID, vJSONstring);
          } else {
            console.log("CALL: loadLS() - parse JSON '"+vLSID+"') from LocalStorage JSONstring='"+vJSONstring.substr(0,120)+"...'");
            try {
                this.aJSON = JSON.parse(vJSONstring);
            } catch(e) {
                alert("ERROR loadLS(): "+ e);
            }
          }
        } else {
          console.log("CALL: loadLS() - JSON-Data '"+vLSID+"' is undefined in Local Storage.\nUse default as JSON");
          //localStorage.setItem(vLSID, JSON.stringify(this.aEditor.getValue()));
        }
    }	 else {
        console.log("WARNING: Sorry, your browser does not support Local Storage of JSON Database. Use Firefox ...");
    }
  };


  this.saveLS = function (pLSID,pJSON) {
    var vLSID = this.url2id() || pLSID || "jsondata";
    console.log("saveLS('"+vLSID+"')-Call");
    var vJSON = pJSON || this.getValue();
    var vJSONstring = "";
    if (typeof(Storage) != "undefined") {
        // Store
        if (typeof(vJSON) != undefined) {
          console.log("LocalStorage: '"+vLSID+"' is defined, JSONDB in  Local Storage");
          if (vJSON) {
            //console.log("pJSONDB '"+vLSID+"' is saved to Local Storage");
            vJSONstring = JSON.stringify(vJSON);
            console.log("LocalStorage: saveLS('"+vLSID+"') JSONstring='"+vJSONstring.substr(0,240)+"...' DONE");
            localStorage.setItem(vLSID,vJSONstring);
          } else {
            console.log("vJSON with JSON is NOT defined");
          }
        } else {
          console.log("JSON Data '"+vLSID+"' is undefined");
        }
      }	 else {
        console.log("WARNING: Sorry, your browser does not support Local Storage of JSON Database. Use Firefox ...");
      }
  };

  this.loadJSON = function () {
    var vThis = this;
    var fileToLoad = this.el(this.aOptions.filejson_id).files[0]; //for input type=file
    if (fileToLoad) {
      console.log("loader4JSON() - File '"+fileToLoad.name+"' exists.");
      $('#load_filename').html(fileToLoad.name); // this.value.replace(/.*[\/\\]/, '')
      var fileReader = new FileReader();
      // set the onload handler
      fileReader.onload = function(fileLoadedEvent){
          var vTextFromFileLoaded = fileLoadedEvent.target.result;
          //document.getElementById("inputTextToSave").value = textFromFileLoaded;
          //alert("textFromFileLoaded="+textFromFileLoaded);
          try {
            var vLoadedJSON = JSON.parse(vTextFromFileLoaded);
            vThis.json2editor(vLoadedJSON);
            vThis.update_filename();
            alert("File JSON '"+fileToLoad.name+"' loaded successfully!");
            vThis.validate_errors();
          } catch(e) {
            vThis.aEditor.setValue(vThis.aDefaultJSON); // Init with an empty class
            alert(e); // error in the above string (in this case, yes)!
          }
        };
      //onload handler set now start loading the file
      fileReader.readAsText(fileToLoad, "UTF-8");
    } else {
      alert("File is missing");
    }
    this.saveLS("jsondata");
  };

  this.json2editor = function(pLoadedJSON) {
    var vJSON = null;
    if (pLoadedJSON) {
      // json2editor has a JSON as paramter
      if (pLoadedJSON.hasOwnProperty('data') && pLoadedJSON.hasOwnProperty('settings')) {
        // load menu DEFINITION AND menu TEMPLATE/layout
        vJSON = pLoadedJSON;
      } else {
        vJSON = cloneJSON(this.aDefaultJSON);
        // here either "data" or "settings" is missing
        if (pLoadedJSON.hasOwnProperty('data')) {
          //  load menu DEFINITION only > settings / layout / templates are missing
          vJSON.data = pLoadedJSON.data;
          console.log("JSON update of 'data' in json2editor()");
        } else if (pLoadedJSON.hasOwnProperty('settings')) {
          // load menu TEMPLATE/layout > data for menu structure is missing
          vJSON.settings = pLoadedJSON.settings;
          console.log("JSON update of 'settings' in json2editor()");
        }
      }
      this.aEditor.setValue(vJSON);
    } else {
      console.error("CALL: json2editor(pLoadedJSON) pLoadedJSON was undefined!");
      this.aEditor.setValue(this.pDefaultJSON);
    }
  };

  this.getAppName4File = function (pJSON) {
    return app2filename(this.getAppName(pJSON),"_menu.json");
  };

  this.getAppName = function (pJSON) {
    if (this.aEditor) {
      console.log("CALL: getAppName() aEditor defined - this.aJSON was set!");
      this.aJSON = this.aEditor.getValue();
    }
    if (pJSON) {
      console.log("CALL: getAppName() initialized with pJSON!");
      this.aJSON = pJSON;
    }
    var vAppName =  this.aJSON.data.appname || "Undefined App";
    return vAppName;
  };

  this.getFilename = function(pJSON) {
    var vAppName = "Undefined App";
    var vExtension = pJSON.settings.extension4json || "_menu.json";
    if (pJSON) {
      if (pJSON.data) {
        if (pJSON.data.appname) {
          vAppName  = pJSON.data.appname;
        } else {
          console.log("WARNING: pJSON.data.appname undefined in JSONEditor4Menu.getFilename()");
        }
      } else {
        console.log("WARNING: pJSON.data undefined in JSONEditor4Menu.getFilename()");
      }
    } else {
      console.log("WARNING: pJSON undefined in JSONEditor4Menu.getFilename()");
    }
    var vFilename = app2filename(vAppName) + vExtension;
    return vFilename;
  };

  this.setAppName = function (pAppName) {
    if (this.aJSON) {
      if (this.aJSON.data) {
        if (this.aJSON.data.appname) {
          this.aJSON.data.appname = pAppName;
        } else {
          console.error("this.aJSON.data.appname is undefined");
        }
      } else {
        console.error("this.aJSON.data is undefined");
      }
    } else {
      console.error("this.aJSON is undefined");
    }
  };


  this.saveJSON = function (pkey) {
    // Get the value from the editor
    //alert("saveJSON()-Call");
    var vPrefix = "";
    var vJSON = this.aEditor.getValue();
    if (pkey) {
      if (vJSON.hasOwnProperty(pkey)) {
        vJSON = {
          pkey : vJSON[pkey]
        };
        vPrefix = pkey + "_";
      }
    }
    this.saveLS("jsondata");
    var vFile = vPrefix + this.getFilename(vJSON);
   // set modified date in reposinfo.modified
    this.update_modified();
    var vContent = JSON.stringify(vJSON,null,4);
    saveFile2HDD(vFile,vContent);
    console.log("JSON output '"+vFile+"':\n"+vContent);
    alert("JSON File: '"+vFile+"' saved!");
  };

  this.saveSchema = function (pOutformatJSON) {
    var vContent = JSON.stringify(this.aSchema,null,4);
    var vExt = ".json";
    var vBasename = "menu_schema";
    if (!pOutformatJSON) {
      vExt = ".js";
      vContent = '// Store "' + vBasename + vExt + '" in "docs/schema/"\n\n' + 'vDataJSON["' + vBasename + '"] = ' + vContent;
    }
    var vFile = vBasename + vExt;
    saveFile2HDD(vFile,vContent);
    console.log("JSON Schema '"+vFile+"' saved!");
    alert("JSON Schema File: '"+vFile+"' saved!");
  };

  this.saveDocumentation = function () {
    // see e.g. template tpl/docu4github_tpl.js
    // stored  vDataJSON["tpl"]["docu4github"]
    this.save4Template("docu4github","_github.md","Github MarkDown Documentation");
  };

  this.viewOutput = function (pContent) {
    //--Textarea Output----------------
    var vOutNode = this.el("tOutput");
    if (vOutNode) {
      vOutNode.value = pContent;
    } else {
      console.log("WARNING: JSONEditor4Menu.viewOutput()-call - textarea 'tOutput' not defined in DOM");
    }
    //---------------------------------
  };

  this.getOutput4Template = function (pTplID) {
      console.log("getOutput4Template('"+pTplID+"')");
      this.update_modified();
      var vJSON = this.aEditor.getValue();
      //-- HandleBars: Compile with javascript-template ---
      // vDataJSON["out"]["javascript"] is HandleBars compiler function
      // Compile functions was generated from "tpl/docu4github_tpl.js"
      var vContent = "Undefined Handlebars Compiler TplID='"+pTplID+"'";
      if (this.compileCode[pTplID]) {
        vContent = this.compileCode[pTplID](vJSON);
        //--Textarea Output----------------
        this.viewOutput(vContent);
        //---------------------------------
      } else {
        console.error("Handlebars4Code template compiler this.compileCode."+pTplID+"() undefined");
      }

      return vContent;
  };

  this.save4Template = function (pTplID,pExtension,pMessage) {
      console.log("save4Template('"+pTplID+"'.'"+pExtension+"','"+pMessage+"')");
      var vMessage = pMessage || "Code for Template generated";
      //vContent = postProcessHandlebars(vContent,vJSON);
      var vContent = this.getOutput4Template(pTplID);
      console.log("save4Template() vContent="+vContent.substr(0,120)+"...");
      //--Textarea Output----------------
      this.viewOutput(vContent);
      //---------------------------------
      //--JSON Output--------------------
      var vJSON = this.getValue();
      var vFile = app2filename(vJSON.data.appname,pExtension);
      saveFile2HDD(vFile,vContent);
      //alert("File '"+vFile+"' saved - "+vMessage);
      console.log("File '"+vFile+"' saved - "+vMessage);
  };



  this.logContentZIP = function (pZIP) {
    pZIP.forEach(function (relativePath, file){
        console.log("File in ZIP: ", relativePath);
    });
  };

  this.setAppFolderZIP = function (pOptions) {
    // app_folder is a global variable -
    var vPosSlash = 0;
    var vAppFolderFound = false;
    this.aZIP.forEach(function (relativePath, file){
      // extract App Folder only once if AppFolder was not found yet.
      if (!vAppFolderFound) {
        vPosSlash = relativePath.indexOf("/");
        if (vPosSlash > 0) {
          //console.log("CALL: getAppFolder(pZIP,pOptions) App Folder found and extracted from file path '"+relativePath+"'");
          vAppFolderFound = true;
          pOptions.app_folder = relativePath.substr(0,vPosSlash); // include the slash in app_folder
          console.log("CALL: getAppFolder(pZIP,pOptions) App Folder '" + pOptions.app_folder + "' found.");

        }
      }
        //vNode.innerHTML = vNode.innerHTML + "<li>" + relativePath + "</li>";
    });
  };

  this.updateFileList4ZIP = function (pID,pOptions) {
    console.log("CALL: updateFileList4ZIP('" + pID + "',pOptions)");
    var vJSON = this.getExpandedJSON(pOptions);
    var vFileList = vJSON.settings[pID];
    var vContent = "";
    var vFilename = "";
    var vTemplate = "";
    var vPath = "";
    for (var i = 0; i < vFileList.length; i++) {
      // generate content for JSON with HandleBars4Code template
      /* e.g. for pID = "jsfiles" vFileList contains array of templates
      jsfiles =
       [
          {
            "filepath": "js/menu.js",
            "istemplate": "yes",
            "code": "// Content of imported Javascript library\n\nThis is the template content of the JavaScript js/menu.js"
          }
       ]
      */
      if (vFileList[i].istemplate != "no") {
        vTemplate = vFileList[i].code;
        var generator4file  = Handlebars4Code.compile(vTemplate);
        vContent = generator4file(vJSON);
        if (!vContent)  {
          vContent = "ERROR: undefined template for '" + pID + i + "' - File: " +  vFileList[i].filepath;
          console.error(vContent);
        }
      } else {
        // file is not a HandleBars4Code template, so store file directly in ZIP
        vContent = vFileList[i].code;
      }
      //console.log("File Content: "+vContent + "\nof file "+JSON.stringify(vFileList[i],null,4));
      // write generated content into ZIP-file 'this.aZIP'
      if (vFileList[i].hasOwnProperty("filepath")) {
        vPath = vFileList[i].filepath;
      } else {
        vPath = "undef_path/" + vID + i + ".txt";
        console.warn("WARNING: Attribute 'filepath' undefined for '" + pID + "' - Debug Filename: '" + vPath + "' ");
      }
      vFilename = pOptions.app_folder + "/" +vPath;
      console.log("Add2ZIP: "+vFilename);
      this.aZIP.file(vFilename, vContent , {base64: false});
    }
  };

  this.addMenuJSON2ZIP = function (pJSON,pOptions) {
    // app2filename(vJSON.data.appname,pExtension);
      var vFilename = pOptions.app_folder + "/db/" + app2filename(pJSON.data.appname,pJSON.settings.extension4json);
      var vContent = JSON.stringify(pJSON,null,4);
      this.aZIP.file(vFilename, vContent , {base64: false});
  };

  this.expandMenuPages4JSON = function (pJSON,pOptions) {
    // app2filename(vJSON.data.appname,pExtension);
    var vMenuList = [];
    if (pJSON.hasOwnProperty("data")) {
      if (pJSON.data.hasOwnProperty("menuitems")) {
        console.log("CALL: expandMenuPages4JSON() - Menu Items found in pJSON ");
        vMenuList = pJSON.data.menuitems;
      } else {
        console.warn("WARNING: 'pJSON.data.menuitems' undefined in expandMenuPages4JSON()");
      }
    } else {
      console.warn("WARNING: 'pJSON.data' undefined in expandMenuPages4JSON()");
    }
    // iterate over menuitems and expand the page content with Handlebars4Code
    for (var i = 0; i < vMenuList.length; i++) {
      if (vMenuList[i].menutype == "page") {
        console.log("Expand Menu Page '" + vMenuList[i].title + "' with expandMenuPages4JSON()");
        var vMenTpl = vMenuList[i].content;
        var generator4file  = Handlebars4Code.compile(vMenTpl);
        vMenuList[i].content = generator4file(pJSON);
      }
    };
    return pJSON;
  };

  this.getExpandedJSON = function (pOptions) {
    // this function expands the Handlebars variables in the
    var vJSON = cloneJSON(this.aEditor.getValue());
    vJSON = this.expandMenuPages4JSON(vJSON,pOptions);
    return vJSON;
  };

  this.addFiles2ZIP = function (pOptions) {
    var vJSON = this.getExpandedJSON(pOptions);
    this.addMenuJSON2ZIP(vJSON,pOptions);
    var vArrID = pOptions.file_list_id || [];
    var vID = "";
    for (var i = 0; i < vArrID.length; i++) {
      vID = vArrID[i];
      if (vJSON.settings.hasOwnProperty(vID)) {
        if (vJSON.settings[vID]) {
          //console.log("CALL: updateFileList4ZIP('" + vID + "',pOptions)");
          this.updateFileList4ZIP(vID,pOptions);
        } else {
          console.error("ERROR: File List '" + vID + "' in vJSON.settings  undefined");
        }
      } else {
        console.error("ERROR: addFiles2ZIP() in JSON settings." + vID + " is undefined");
      }
    }
  };

  this.exportAppZIP = function (pOptions) {
    //this.logContentZIP(pZIP);
    this.setAppFolderZIP(pOptions);
    this.addFiles2ZIP(pOptions);
    var vFilename = pOptions.app_folder + ".zip";
    this.aZIP.generateAsync({type:"blob"}).then(function (blob) { // 1) generate the zip file
      alert("ZIP file '"+pOptions.app_folder + ".zip' generated.\nZIP-file contains the generate WebApp with the defined menu!");
      saveAs(blob, vFilename);                          // 2) trigger the download
      console.log("Save ZIP File: '" + vFilename + "'");
    }, function (err) {
      console.error("ERROR: generation of zip-file '" + vFilename + "' - "+err);
    });
  };



  this.save4ZIP = function (pOptions) {
    if (isHash(pOptions)) {
      // vArrID resp. pOptions.file_list_id contains the ID for the handlebars4code templates or raw files
      var vArrID = ["htmlfiles","cssfiles","jsfiles"];
      // for output generate an array ID string
      var vArrID_string = "['" + vArrID.join("','") + "']";
      // vArrID_string:  ['htmlfiles','cssfiles','jsfiles']
      if (pOptions.hasOwnProperty("file_list_id")) {
          console.log("CALL save4ZIP(): pOptions.file_list_id exists");
          vArrID_string = "['" + pOptions.file_list_id.join("','") + "']";
      } else {
          console.log("WARNING save4ZIP(): pOptions.file_list_id undefined - use default " + vArrID_string);
          pOptions.file_list_id = vArrID;
      }
      // check if an output message is defined in pOptions - otherwise set default message
      if (!pOptions.hasOwnProperty("message")) {
        console.log("Output Message defined: '" + pOptions.message + "'");
      } else {
        pOptions.message = "ZIP for WebApp with Menue was generated";
      }
      console.log("CALL: save4ZIP(pZIP,pOptions) with file list IDs=" +vArrID_string + " exported with Handlebars4Code");
      this.exportAppZIP(pOptions);
    } else {
      console.error("WARNING save4ZIP(pZIP): pOptions was not provided as parameter");
    }

  };


  this.saveCode = function (pTplID,pExt,pMessage) {

    var vTplID = pTplID || "javascript";
    var vExt = pExt || ".js";
    var vMessage = pMessage || "Javascript Code for Class";
    // see e.g. template tpl/javascript_class_tpl.js
    // stored  vDataJSON["tpl"]["javascript"]
    this.save4Template(vTplID,vExt,vMessage);
  };



} // end JSONEditor4Menu


module.exports = JSONEditor4Menu;
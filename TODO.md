DEVELOPMENT Performed jsoneditor4menu not in hamburger_menu_app or unity_like_menu_app

ClassEditorUML is just using jsoneditor4menu with a specific JSON Schema

!!! DO NOT DEVELOP IN hamburger_menu_app !!!
Just adapt Schema here e.g. for ReactJS

BUG: ZIP generation does updates the CSS with values.
NEW: add the unity_like_menu_app to JSONEditor4Menu
NEW: add LS-Save to Load and Save  repository.
BUG: Does not update the classes list after alteration
SOLUTION: Update Classes not internally but by LinkParam insert new classes in JSON.

Add in analogy to icon fonts SVG files as Icon option
```javascript
JSONEditor.defaults.iconlibs.foundation3 = JSONEditor.AbstractIconLib.extend({
  mapping: {
    collapse: 'minus',
    expand: 'plus',
    "delete": 'x',
    edit: 'pencil',
    add: 'page-add',
    cancel: 'x-circle',
    save: 'save',
    moveup: 'arrow-up',
    movedown: 'arrow-down'
  },
  icon_prefix: 'fi-'
});
```

```javascript
JSONEditor.defaults.iconlibs.icons4menu = JSONEditor.AbstractIconLib.extend({
  mapping: {
    collapse: 'minus',
    expand: 'plus',
    "delete": 'x',
    edit: 'pencil',
    add: 'page-add',
    cancel: 'x-circle',
    save: 'save',
    moveup: 'arrow-up',
    movedown: 'arrow-down'
  },
  icon_prefix: 'img/icons-svg/',
  icon_postfix: '_white.svg'
});
```

Overwrite `getIconClass()` and `getIcon()` in the JSON Editor l.7558.

```javascript
getIconClass: function(key) {
  if(this.mapping[key]) return this.icon_prefix+this.mapping[key];
  else return null;
},
getIcon: function(key) {
  var iconclass = this.getIconClass(key);

  if(!iconclass) return null;

  var i = document.createElement('i');
  i.className = iconclass;
  return i;
}
});
```

like


```javascript
getIconClass: function(key) {
  if(this.mapping[key]) return this.icon_prefix+this.mapping[key];
  else return null;
},
getIcon: function(key) {
  // check if
  var iconclass = this.getIconClass(key);

  if(!iconclass) return null;

  var i = document.createElement('i');
  i.className = iconclass;
  return i;
}
});
```

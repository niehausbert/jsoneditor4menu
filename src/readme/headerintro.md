
<!-- BEGIN: src/readme/headerinto.md -->
Generate WebApp with responsive web design:
<center>
  <h3>[Demo: ___PKG_EXPORTVAR___](___PKG_URL4WWW___)</h3>
</center>

<img src="images/mobile_menu.png" width="360" alt="Mobile View of WebApp">

<img src="images/desktop_menu.png" width="460" alt="Desktop View of WebApp">
<!--
![Desktop View of WebApp](images/desktop_menu.png)
![Mobile View of WebApp](images/mobile_menu.png)
-->

JSONEditor4Menu can be used to create such [AppLSACs](https://en.wikiversity.org/wiki/AppLSAC) in a [Responsive Web Design](https://en.wikipedia.org/wiki/Responsive_web_design).

The following table of contents is generated with `node doctoc README.md`.
<!-- END:   src/readme/headerinto.md -->

JSONEditor.defaults.iconlibs.icons4menu = JSONEditor.AbstractIconLib.extend({
  mapping: {
    collapse: 'minus-black',
    expand: 'caret-r',
    "delete": 'fa-trash',
    edit: 'edit-black',
    add: 'plus-black',
    cancel: 'caret-l-black',
    save: 'fa-save-file',
    moveup: 'arrow-u-black',
    movedown: 'arrow-d-black'
  },
  icon_prefix: 'img/icons-svg/',
  icon_postfix: '.svg',

  getIconURL: function(key) {
    if(this.mapping[key]) return this.icon_prefix+this.mapping[key]+this.icon_postfix;
    else return null;
  },
  getIcon: function(key) {
    var icon_url = this.getIconURL(key);
    console.log("Icon URL: "+icon_url);

    if(!icon_url) return null;

    var i = document.createElement('img');
    i.className = "icon4menu";
    i.src = icon_url;
    return i;
  }
});

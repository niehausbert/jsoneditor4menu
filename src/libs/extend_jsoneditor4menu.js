JSONEditor4Menu.prototype.get_template4file = function (pFileList,pFilename) {
  /*
     CALL: get_template() fetches the template and
     provides a filehash of the template
 */
  var vJSON = this.getValue();
  var tpl = {
    "filepath": "undefined.html",
    "istemplate":"no",
    "code": "Template '" + pFileList+"' in file '" + pFilename+  "' undefined!"
  };
  // expand pages for the menuitems - the may contain JSON variables
  var fl = null;
  var vFound = false;
  if (vJSON.hasOwnProperty("data")) {
    if (vJSON.data.hasOwnProperty("menuitems")) {
      fl = vJSON.data.menuitems;
      for (var i = 0; i < fl.length; i++) {
        if (fl[i].menutype == "page") {
          var vTemplate = fl[i].content;
          var generator4file = Handlebars4Code.compile(vTemplate);
          fl[i].content = generator4file(vJSON);
        }
      }
    } else {
      console.error("vJSON.data.menuitems undefined in call: vJSONEditor4Menu.get_template4file()");
    }
  } else {
    console.error("vJSON.data undefined in call: vJSONEditor4Menu.get_template4file()");
  };
  // expand the filelist with HandleBars4Code for CSS and JS
  if (vJSON.hasOwnProperty("settings")) {
    if (vJSON.settings.hasOwnProperty(pFileList)) {
      fl = vJSON.settings[pFileList];
      // e.g. fl = settings.jsfiles
      for (var i = 0; i < fl.length; i++) {
        // check if the template with the filename exists in the filelist
        if (fl[i].filepath == pFilename) {
          // template found
          tpl.filepath = fl[i].filepath;
          tpl.istemplate = fl[i].istemplate;
          tpl.code = fl[i].code;
          vFound = true;
          console.log("Template '" + pFilename+  "' found in filelist '" + pFileList+"'.");
        }
      };
    } else {
      console.error("ERROR: JSON.settings has no file list '" + pFileList + "'.");
    }
  } else {
    console.error("ERROR: data in JSON has no 'settings' attribute");
  };
  if (vFound == false) {
    console.error("ERROR: Template not found!");
  };

  return tpl;
}

JSONEditor4Menu.prototype.replace_file_iteration = function (tpl) {
  var vSearch = "<link rel=\"stylesheet\" href=\"{{{filepath}}}\">";
  var vReplace = "<style>{{{code}}}</style>";
  if (tpl.code.indexOf(vSearch) >= 0) {
    tpl.code = replaceString(tpl.code,vSearch,vReplace);
  } else {
    console.warn("Template Replace in '" + tpl.filepath + "': Search string: '" + vSearch + "' not found");
  };
  vSearch = "script src=\"{{{filepath}}}\">";
  vReplace = "script>{{{code}}}";
  if (tpl.code.indexOf(vSearch) >= 0) {
    tpl.code = replaceString(tpl.code,vSearch,vReplace);
  } else {
    console.warn("Template Replace in '" + tpl.filepath + "': Search string: '" + vSearch + "' not found");
  };
}

JSONEditor4Menu.prototype.templates2files = function (pJSON) {
  var vJSON = pJSON || this.aEditor.getValue();
  var fl_id = ["cssfiles","jsfiles"];
  for (var i = 0; i < fl_id.length; i++) {
    var vID = fl_id[i];
    var vFileList = vJSON.settings[vID];
    for (var k = 0; k < vFileList.length; k++) {
      vTPL = vFileList[k];
      if (vTPL.istemplate != "no") {
        var vTemplate = vTPL.code;
        var generator4file = Handlebars4Code.compile(vTemplate);
        vTPL.code = generator4file(vJSON);
      }; // replace only templates - no need to replace static files
    }; // for template in filelist iteration
  }; // for filelist id iteration
  return vJSON;
};

JSONEditor4Menu.prototype.get_onepage_html = function (tpl,pOptions) {

  // replace template for index.html and generate a one-page WebApp
  console.log("CALL: get_onepage_html('" + tpl.filepath + "',pOptions)");

  var vJSON = this.getExpandedJSON(pOptions);
  //var vJSON = this.aEditor.getValue();
  console.log("JSON with getExpandedJSON(): \n"+JSON.stringify(vJSON,null,4));
  if (tpl) {
    console.log("CALL: get_onepage_html(tpl,pOptions) - tpl defined");
  } else {
    console.warn("WARNING: 'tpl' undefined in call of get_onepage_html(tpl,pOptions)");
  };
  // CSS Replace import <link  > by <style> </style>
  // replace <link rel="stylesheet" href="{{{filepath}}}">
  pTpl = this.replace_file_iteration(tpl);
  vJSON = this.templates2files(vJSON);
  var vContent = "";
  var vFilename = "";
  var vTemplate = "";
  var vPath = "";
  if (tpl.istemplate != "no") {
    var vTemplate = tpl.code;
    var generator4file = Handlebars4Code.compile(vTemplate);
    vContent = generator4file(vJSON);
    if (!vContent)  {
      vContent = "ERROR: undefined template for '" + tpl.filepath + "' - File: " +  tpl.filepath;
      console.error(vContent);
    };
    // vContent contains still Handlebars marker in the replaced JS files
    //generator4file  = Handlebars4Code.compile(vContent);
    //Content = generator4file(vJSON);
  } else {
    // file is not a HandleBars4Code template, so store file directly in ZIP
    vContent = tpl.code || "undefined code";
  }

  return vContent;
};

JSONEditor4Menu.prototype.get_multifile_html = function (tpl) {
  console.log("CALL: get_multifile_app('" + tpl.filepath + "')");
  // get the HTML code for the one-page WebApp for index.html template
  var generator4file  = Handlebars4Code.compile(tpl.code);
  vContent = generator4file(vJSON);

  return vContent;
};

JSONEditor4Menu.prototype.get_app_source = function (pOptions) {
  var tpl_hash = this.get_template4file("htmlfiles","index.html");
  // get the HTML code for the one-page WebApp for index.html template
  //console.log("tpl_hash="+JSON.stringify(tpl_hash,null,4));
  var html = this.get_onepage_html(tpl_hash,pOptions);
  //var html = this.get_multifile_html(tpl_hash);
  document.getElementById("tOutput").value = html;
  //html += "<html><body><h1>App Preview</h1>Check the App in Preview Mode.</body></html>";


  return html;
};

JSONEditor4Menu.prototype.openAppPreview = function (pOptions) {
  console.log("App Preview in HTML window.");
  var vOptions = pOptions || {};
  var app_preview_win = window.open("","wPAppWin","width=400,height=600,scrollbars=1,resizable=1");
  var html = this.get_app_source(pOptions);
  if (vOptions.hasOwnProperty("debug_output")) {
      var vNode = document.getElementById(vOptions.debug_output);
      if (vNode) {
        vNode.value = html;
      }
  };
  // Open a App Preview window "app_preview_win"
  app_preview_win.document.open();
  app_preview_win.document.write(html);
  app_preview_win.document.close();
  app_preview_win.focus();
  //app_preview_win.print();
  //app_preview_win.close();
};


## Libraries required for  `JSONEditor4Menu`
The following libraries are necessary for `jsoneditor4menu.js`:
* Lib: `jquery` Version: `^3.4.1`
* Lib: `linkparam` Version: `^1.0.8`


## Libraries for Building and Developement
The following libraries are necessary for building the `jsoneditor4menu`. 
These libraries are not included in `jsoneditor4menu.js`, but e.g. are required in `build.js`.
* Lib: `build4code` Version: `^0.3.27`
* Lib: `concat-files` Version: `^0.1.1`
* Lib: `doctoc` Version: `^1.3.0`
* Lib: `jsdom` Version: `^13.2.0`
* Lib: `shelljs` Version: `^0.8.3`
* Lib: `uglify-js` Version: `^3.6.0`


/* ---------------------------------------
 Exported Module Variable: JSONEditor4Menu
 Package:  jsoneditor4menu
 Version:  1.1.26  Date: 2021/05/14 15:21:49
 Homepage: https://niehausbert.gitlab.io/jsoneditor4menu
 Author:   Engelbert Niehaus
 License:  MIT
 Date:     2021/05/14 15:21:49
 Require Module with:
    const JSONEditor4Menu = require('jsoneditor4menu');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */

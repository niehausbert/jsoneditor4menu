## NPM Library Information
* Exported Module Variable: `JSONEditor4Menu`
* Package:  `jsoneditor4menu`
* Version:  `1.1.26`   (last build 2021/05/14 15:21:49)
* Homepage: `https://niehausbert.gitlab.io/jsoneditor4menu`
* License:  MIT
* Date:     2021/05/14 15:21:49
* Require Module with:
```javascript
    const vJSONEditor4Menu = require('jsoneditor4menu');
```
* JSHint: installation can be performed with `npm install jshint -g`
